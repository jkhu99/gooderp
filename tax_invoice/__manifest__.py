# -*- coding: utf-8 -*-
{
    'name': "GOODERP 发票管理",
    'author': "永远的不知",
    'website': "https://www.osbzr.com",
    'category': 'gooderp',
    "description":
    '''
                        该模块实现发票管理的基础内容。
    ''',
    'version': '10.0.0.1',
    'depends': ['buy', 'sell'],
    'data': [
        'security/ir.model.access.csv',
        'security/rules.xml',
        'view/tax_invoice_view.xml',
        'view/tax_invoice_reconcile_view.xml',
        'view/tax_invoice_track_view.xml',
        'view/partner_view.xml',
        'view/buy_view.xml',
        'view/sell_view.xml',
        'view/money_view.xml',
        'view/goods_view.xml',
        'wizard/tax_invoice_track_wizard_view.xml',
    ],
    'demo': [
        # 'data/demo.xml',
    ],
    'post_init_hook': 'set_draft_invoice_hook',
}
