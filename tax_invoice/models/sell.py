# -*- coding: utf-8 -*-

from odoo import fields, models, api
import odoo.addons.decimal_precision as dp

class SellOrder(models.Model):
    _inherit = "sell.order"

    tax_invoice_ids = fields.One2many('tax.invoice','sell_order_id', string = u'发票号',
                                     readonly=True,
                                     help= u'结算单对应的发票')
    tax_invoice_count = fields.Integer(
        compute='_compute_tax_invoice', string=u'发票数量', default=0)

    @api.depends('tax_invoice_ids')
    def _compute_tax_invoice(self):
        for order in self:
            order.tax_invoice_count = len(
                [tax_invoice for tax_invoice in order.tax_invoice_ids])

    @api.multi
    def action_view_tax_invoice(self):
        '''
        This function returns an action that display existing picking orders of given purchase order ids.
        When only one found, show the picking immediately.
        '''
        self.ensure_one()
        tax_invoice_ids = [tax_invoice.id for tax_invoice in self.tax_invoice_ids]
        # choose the view_mode accordingly
        action = self.env.ref(
            'tax_invoice.out_tax_invoice_action').read()[0]
        if len(tax_invoice_ids) > 1:
            action['domain'] = "[('id','in',[" + \
                ','.join(map(str, tax_invoice_ids)) + "])]"
        elif len(tax_invoice_ids) == 1:
            view_id = self.env.ref('tax_invoice.out_tax_invoice_form').id
            action['views'] = [(view_id, 'form')]
            action['res_id'] = tax_invoice_ids and tax_invoice_ids[0] or False
        return action
