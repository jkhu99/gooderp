# -*- coding: utf-8 -*-
from odoo import fields, models, api
from odoo.exceptions import UserError


class MoneyInvoice(models.Model):
    _inherit = 'money.invoice'

    tax_invoice_ids = fields.One2many('tax.invoice', 'money_invoice_id', string=u'发票号',
                                      readonly=True,
                                      help=u'结算单对应的发票')
    tax_invoice_count = fields.Integer(
        compute='_compute_tax_invoice', string=u'发票数量', readonly=True, default=0)

    sell_order_id = fields.Many2one('sell.order', u'销售订单',
                                    ondelete='restrict', readonly=True,
                                    help=u'与结算单相关的销售订单号')

    buy_order_id = fields.Many2one('buy.order', u'采购订单',
                                   ondelete='restrict', readonly=True,
                                   help=u'与结算单相关的采购订单号')

    @api.depends('tax_invoice_ids')
    def _compute_tax_invoice(self):
        for order in self:
            order.tax_invoice_count = len(
                [tax_invoice for tax_invoice in order.tax_invoice_ids])

    @api.multi
    def recreate_partner_tax_invoice_by_button(self):
        """
        根据结算单重新生成相关partner处于草稿状态的发票
        :return:
        """
        for invoice in self.env['money.invoice'].search([('partner_id', '=', self.partner_id.id)]):
            # if invoice.to_invoice_amount != 0:
            #     if invoice.state == 'done':
            #         invoice.money_invoice_draft()
            unlinklist=[]
            for tax_invoice in invoice.tax_invoice_ids:
                if(tax_invoice.state == 'draft'):
                    unlinklist.append(tax_invoice)
            for tax_invoice in unlinklist:
                tax_invoice.unlink()        
        invoice_ids = self.env['money.invoice'].search(
            [('partner_id', '=', self.partner_id.id), ('tax_invoice_ids', '=', False)])
        for invoice in invoice_ids:
            if not invoice.tax_invoice_ids:
                invoice.to_invoice_amount = invoice.amount
            else:
                taxed_amount = 0
                for tax in invoice.tax_invoice_ids:
                    taxed_amount += tax.invoice_subtotal
                invoice.to_invoice_amount = invoice.amount - taxed_amount
            invoice._generate_tax_invoice()

    @api.multi
    def recreate_money_invoice_tax_invoice_by_button(self):
        """
        根据结算单重新生成相关结算单处于草稿状态的发票
        :return:
        """
        # if self.to_invoice_amount != 0:
        #     if self.state == 'done':
        #         self.money_invoice_draft()
        unlinklist=[]
        for tax_invoice in self.tax_invoice_ids:
            if(tax_invoice.state == 'draft'):
                unlinklist.append(tax_invoice)

        for tax_invoice in unlinklist:
            tax_invoice.unlink()
        if not self.tax_invoice_ids:
            self.to_invoice_amount = self.amount
        else:
            taxed_amount = 0
            for tax in self.tax_invoice_ids:
                taxed_amount += tax.invoice_subtotal
            self.to_invoice_amount = self.amount - taxed_amount
        self._generate_tax_invoice()

    @api.multi
    def action_view_tax_invoice(self):
        '''
        This function returns an action that display existing picking orders of given purchase order ids.
        When only one found, show the picking immediately.
        '''

        self.ensure_one()
        tax_invoice_ids = [
            tax_invoice.id for tax_invoice in self.tax_invoice_ids]
        # choose the view_mode accordingly

        if self.category_id.type != 'income':
            action = self.env.ref(
                'tax_invoice.in_tax_invoice_action').read()[0]
            if len(tax_invoice_ids) > 1:
                action['domain'] = "[('id','in',[" + \
                    ','.join(map(str, tax_invoice_ids)) + "])]"
            elif len(tax_invoice_ids) == 1:
                view_id = self.env.ref('tax_invoice.in_tax_invoice_form').id
                action['views'] = [(view_id, 'form')]
                action['res_id'] = tax_invoice_ids and tax_invoice_ids[0] or False
        else:
            action = self.env.ref(
                'tax_invoice.out_tax_invoice_action').read()[0]
            if len(tax_invoice_ids) > 1:
                action['domain'] = "[('id','in',[" + \
                    ','.join(map(str, tax_invoice_ids)) + "])]"
            elif len(tax_invoice_ids) == 1:
                view_id = self.env.ref('tax_invoice.out_tax_invoice_form').id
                action['views'] = [(view_id, 'form')]
                action['res_id'] = tax_invoice_ids and tax_invoice_ids[0] or False
        return action

    order_id = fields.Char(u'订单号', readonly=True)
    invoice_date = fields.Date(u'开票日期')
    invoice_done_amount = fields.Float(u'已开票金额', readonly=True)
    to_invoice_amount = fields.Float(u'未开票金额', readonly=True)
    ref = fields.Char(u'外部订单号', readonly=True)

    def _get_tax_invoice_line(self, line):
        '''返回发票单行'''
        quantity = line.goods_qty - line.quantity_invoice_done
        amount = line.tax_rate * quantity
        '''返回发票单行'''
        return {
            'goods_id': line.goods_id.id,
            'goods_name': line.goods_id.name,
            'quantity': quantity,
            'uom': line.uom_id.name,
            'price': line.price_taxed,
            'tax_rate': line.tax_rate,
            'amount': line.subtotal,
            'tax': line.tax_amount,
            'wh_move_line_id': line.id
        }

    def _get_tax_invoice_goods_id(self, goods_id):
        '''返回发票单行'''
        tax_amount = self.amount - self.amount/(1+goods_id.tax_rate)
        return {
            'goods_id': goods_id.id,
            'goods_name': goods_id.name,
            'quantity': 1,
            'uom': goods_id.uom_id.name,
            'price': self.amount,
            'tax': self.tax_amount,
            'tax_rate': self.amount/(self.amount - self.tax_amount)*100 -100,
            'amount': self.amount,
            'wh_move_line_id': ''
        }        

    def _generate_tax_invoice(self):
        '''返回创建 tax_invoice 时所需数据'''
        my_company = self.create_uid.company_id
        if not my_company.vat or not my_company.company_registry or not my_company.bank_account_id.num:
            raise UserError(u'公司的注册地址、税号、开户行账户，不能为空')

        if not self.partner_id.tax_num or not self.partner_id.main_address or not self.partner_id.bank_name or not self.partner_id.bank_num:
            raise UserError(u'合作伙伴：%s 的注册地址、税号、开户行账户、账号，不能为空' % (self.partner_id.name))

        tax_invoice_lines = []  # 发货单行
        if self.category_id.type != 'income':
            invoice_type = 'in'
            if not self.buy_order_id:
                receipt = self.env['buy.receipt'].search(
                    [('name', '=', self.name)])
                self.buy_order_id = receipt.order_id
            order_id = self.buy_order_id
            for line in self.move_id.line_in_ids:
                # 如果订单部分开票，则点击此按钮时生成剩余数量的发票草稿
                to_done = line.goods_qty - line.quantity_invoice_done
                if to_done <= 0:
                    continue
                tax_invoice_lines.append(
                    self._get_tax_invoice_line(line))
            if tax_invoice_lines:
                tax_invoice_id = self.env['tax.invoice'].create({
                    'name': self.name,
                    'partner_id': self.partner_id.id,
                    'partner_code': self.partner_id.tax_num,
                    'partner_address': self.partner_id.main_address,
                    'partner_bank_number':
                        (self.partner_id.bank_name +
                         ' ' + self.partner_id.bank_num),
                    'buy_order_id': order_id.id,
                    'partner_order': self.ref,
                    'catagory': self.partner_id.tax_catagory,
                    'money_invoice_id': self.id,
                    'invoice_type': invoice_type,
                    'my_company_name': my_company.name,
                    'my_company_code': my_company.vat,
                    'my_company_address': my_company.company_registry,
                    'my_company_bank_number': my_company.bank_account_id.name +
                    my_company.bank_account_id.num
                })
                tax_invoice_id.write({'line_ids': [
                    (0, 0, line) for line in tax_invoice_lines]})
        else:
            invoice_type = 'out'
            if not self.sell_order_id:
                delivery = self.env['sell.delivery'].search(
                    [('name', '=', self.name)])
                self.sell_order_id = delivery.order_id
            order_id = self.sell_order_id
            for line in self.move_id.line_out_ids:
                # 如果订单部分开票，则点击此按钮时生成剩余数量的发票草稿
                to_done = line.goods_qty - line.quantity_invoice_done
                if to_done <= 0:
                    continue
                tax_invoice_lines.append(
                    self._get_tax_invoice_line(line))
            if tax_invoice_lines:
                tax_invoice_id = self.env['tax.invoice'].create({
                    'name': self.name,
                    'partner_id': self.partner_id.id,
                    'partner_code': self.partner_id.tax_num,
                    'partner_address': self.partner_id.main_address,
                    'partner_bank_number':
                        (self.partner_id.bank_name +
                         ' ' + self.partner_id.bank_num),
                    'sell_order_id': order_id.id,
                    'partner_order': self.ref,
                    'catagory': self.partner_id.tax_catagory,
                    'money_invoice_id': self.id,
                    'invoice_type': invoice_type,
                    'my_company_name': my_company.name,
                    'my_company_code': my_company.vat,
                    'my_company_address': my_company.company_registry,
                    'my_company_bank_number': my_company.bank_account_id.name +
                    my_company.bank_account_id.num
                })
                tax_invoice_id.write({'line_ids': [
                    (0, 0, line) for line in tax_invoice_lines]})
        if self.note == u'委托加工':
            goods_id = self.env['goods'].search(
                [('name', '=', u'委外加工')])
            if not goods_id:
                raise UserError(u'"委外加工"商品尚未建立')
            else:
                tax_invoice_lines.append(
                    self._get_tax_invoice_goods_id(goods_id))                  
                tax_invoice_id = self.env['tax.invoice'].create({
                    'name': self.name,
                    'partner_id': self.partner_id.id,
                    'partner_code': self.partner_id.tax_num,
                    'partner_address': self.partner_id.main_address,
                    'partner_bank_number':
                        (self.partner_id.bank_name +
                         ' ' + self.partner_id.bank_num),
                    'partner_order': self.ref,
                    'catagory': self.partner_id.tax_catagory,
                    'money_invoice_id': self.id,
                    'invoice_type': invoice_type,
                    'my_company_name': my_company.name,
                    'my_company_code': my_company.vat,
                    'my_company_address': my_company.company_registry,
                    'my_company_bank_number': my_company.bank_account_id.name +
                    my_company.bank_account_id.num
                })            
                tax_invoice_id.write({'line_ids': [
                    (0, 0, line) for line in tax_invoice_lines]})
        if tax_invoice_id:
            if not tax_invoice_lines:
                raise UserError(u'结算单%s的发票明细行不能为空' % (self.name))

    @api.model
    def create(self, values):
        """
        创建结算单时，创建对应的税务发票。
        """
        new_id = super(MoneyInvoice, self).create(values)
        new_id.to_invoice_amount = new_id.amount
        new_id._generate_tax_invoice()
        return new_id

    def unlink(self):
        """
        只允许删除未审核的单据
        :return:
        """
        # 删除已生成的税务发票
        if self.tax_invoice_ids:
            self.tax_invoice_ids.unlink()

        return super(MoneyInvoice, self).unlink()


class MoneyOrder(models.Model):
    _inherit = 'money.order'
    _description = u"收付款单"

    def _get_source_line(self, invoice):
        """
        根据传入的invoice的对象取出对应的值 构造出 source_line的一个dict 包含source line的主要参数
        :param invoice: money_invoice对象
        :return: dict
        """
        return {
            'name': invoice.id,
            'ref': invoice.ref,
            'category_id': invoice.category_id.id,
            'amount': invoice.amount,
            'date': invoice.date,
            'reconciled': invoice.reconciled,
            'invoice_done_amount': invoice.invoice_done_amount,
            'to_reconcile': invoice.to_reconcile,
            'this_reconcile': invoice.to_reconcile,
            'date_due': invoice.date_due,
        }


class SourceOrderLine(models.Model):
    _inherit = 'source.order.line'
    _description = u'待核销行'

    invoice_done_amount = fields.Float(u'已开票金额', readonly=True)
