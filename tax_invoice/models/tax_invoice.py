# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2020  永远的不知().
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundaption, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from odoo import fields, models, api
from odoo.exceptions import UserError
import odoo.addons.decimal_precision as dp

# 状态可选值
TAX_INVOICE_STATES = [
    ('draft', u'待开具'),
    ('submit', u'已通知开票'),
    ('done', u'已完成')]

READONLY_STATES = {
    'done': [('readonly', True)],
    'cancel': [('readonly', True)],
}

class TaxInvoice(models.Model):
    _name = 'tax.invoice'
    _description = u'发票'
    _rec_name = 'money_invoice_id'
    _order = 'invoice_date DESC'
    date = fields.Date(u'单据日期',
                       required=True,
                       states=READONLY_STATES,
                       default=lambda self: fields.Date.context_today(self),
                       index=True,
                       copy=False,
                       help=u"默认是创建日期")
    state = fields.Selection(TAX_INVOICE_STATES, u'状态', readonly=True,
                             help=u"发票状态标识，新建时状态为草稿，确认后状态为完成", index=True,
                             copy=False, default='draft')

    money_invoice_id = fields.Many2one('money.invoice', u'结算单', readonly=True,
                                       ondelete='cascade', help=u'发票对应的结算单')

    my_company_name = fields.Char(u'本公司名称', readonly=True, help=u'本公司名称')
    my_company_code = fields.Char(u'纳税人税号', readonly=True)
    my_company_address = fields.Char(u'地址、电话', readonly=True)
    my_company_bank_number = fields.Char(u'开户行及帐号', readonly=True)

    partner_id = fields.Many2one('partner', u'业务伙伴', ondelete='restrict',
                                 readonly=True, help=u'该单据对应的业务伙伴')
    partner_code = fields.Char(u'纳税人税号', readonly=True)
    partner_address = fields.Char(u'地址、电话', readonly=True)
    partner_bank_number = fields.Char(u'开户行及帐号', readonly=True)
    partner_order = fields.Char(u'合作伙伴订单号', readonly=True)
    sell_order_id = fields.Many2one('sell.order', u'销售订单',
                                    ondelete='restrict',readonly=True,
                                    help=u'与发票相关的销售订单号')
    buy_order_id = fields.Many2one('buy.order', u'采购订单',
                                   ondelete='restrict',readonly=True,
                                   help=u'与结算单相关的采购订单号')
    name = fields.Char(string=u'内部序号', copy=False,
                       ondelete='cascade', readonly=True,
                       help=u'内部发票序号')

    invoice_type = fields.Selection([('in', u'进项发票'),
                                     ('out', u'销项发票')], u'发票种类', readonly=True)
    catagory = fields.Selection([('pt', u'增值税普通发票'),
                                 ('zy', u'增值税专用发票')], u'发票类型', readonly=True)
    invoice_code = fields.Char(u'发票代码', help=u'该单据对应的发票代码')
    invoice_number = fields.Char(u'发票号码', help=u'发票号码，多张发票中间用空格分开')
    
    @api.one
    @api.depends('line_ids.tax', 'line_ids.amount')
    def _compute_all_amount(self):
        self.invoice_subtotal = sum(line.amount for line in self.line_ids)
        self.invoice_tax = sum(line.tax for line in self.line_ids)
        self.invoice_amount = self.invoice_subtotal - self.invoice_tax

    invoice_amount = fields.Float(u'合计金额', compute=_compute_all_amount,
                          store=True, readonly=True,
                          digits=dp.get_precision('Amount'))
    invoice_tax = fields.Float(u'合计税额', compute=_compute_all_amount,
                          store=True, readonly=True,
                          digits=dp.get_precision('Amount'))
    invoice_subtotal = fields.Float(u'价税合计', compute=_compute_all_amount,
                          store=True, readonly=True,
                          digits=dp.get_precision('Amount'))
    invoice_date = fields.Date(u'开票日期')
    line_ids = fields.One2many('tax.invoice.line', 'tax_invoice_id', u'发票明细行',                                
                                states=READONLY_STATES,copy=True)
    attachment_number = fields.Integer(
        compute='_compute_attachment_number', string=u'附件号')
    note = fields.Text(u"备注")

    _sql_constraints = [
        ('unique_invoice_code_number',
         'unique(money_invoice_id, invoice_code, invoice_number)',
         u'结算单+发票代码+发票号码不能相同!'),
    ]

    def action_get_attachment_view(self):
        res = self.env['ir.actions.act_window'].for_xml_id(
            'base', 'action_attachment')
        res['domain'] = [('res_model', '=', 'tax.invoice'),
                         ('res_id', 'in', self.ids)]
        res['context'] = {'default_res_model': 'tax.invoice',
                          'default_res_id': self.id}
        return res

    def _compute_attachment_number(self):
        attachment_data = self.env['ir.attachment'].read_group(
            [('res_model', '=', 'tax.invoice'),
             ('res_id', 'in', self.ids)],
            ['res_id'], ['res_id'])
        attachment = dict((data['res_id'], data['res_id_count'])
                          for data in attachment_data)
        self.attachment_number = attachment.get(self.id, 0)

    def tax_invoice_submit(self):
        '''提交发票'''
        self.ensure_one()
        if self.state == 'submit':
            raise UserError(u'请不要重复提交！')
        self.write({
            'state': 'submit',
        })

    def write(self, vals):
        if (vals.get('state') == 'done' or self.state == 'done'):
            if (not vals.get('invoice_number')) and (not self.invoice_number):
                raise UserError(u'发票号码不能为空')
            if vals.get('invoice_number') :
                for number in vals.get('invoice_number').split(' '):
                    if len(number) != 8:
                        raise UserError(u'发票号码 %s 长度为 %s ,应该为8' % (number, len(number)))

            if not vals.get('invoice_date') and (not self.invoice_date):
                raise UserError(u'开票日期不能为空')
        super(TaxInvoice, self).write(vals)   
        #在done状态，如果发票号码填错了，或者发票开错了重开，需要改发票号码，所以发票更新结算单的情况统一成下面算法
        if self.state == 'done':
            bill_number = ''
            for tax_invoice in self.money_invoice_id.tax_invoice_ids:
                if bill_number:
                    bill_number += ', ' + tax_invoice.invoice_number 
                else:           
                    bill_number = tax_invoice.invoice_number
            self.money_invoice_id.write({
            'bill_number': bill_number
        })
        return True

    def tax_invoice_draft(self):
        '''审核时不合法的给出报错'''
        self.ensure_one()
        if self.state == 'done':
            self.write({
                'state': 'draft',
            })
        if self.money_invoice_id:
            for line in self.line_ids: 
                quantity_invoice_done = line.wh_move_line_id.quantity_invoice_done - line.quantity
                line.wh_move_line_id.write({'quantity_invoice_done': quantity_invoice_done})
        invoice_done_amount = self.money_invoice_id.invoice_done_amount - self.invoice_subtotal
        to_invoice_amount = self.money_invoice_id.amount + invoice_done_amount

        self.money_invoice_id.write({
            'invoice_done_amount': invoice_done_amount,
            'to_invoice_amount': to_invoice_amount,
            'date': self.invoice_date,
        })

    def tax_invoice_done(self):
        '''审核时不合法的给出报错'''
        self.ensure_one()
        if self.state == 'done':
            raise UserError(u'请不要重复确认！')
        self.write({
            'state': 'done',
        })
        #
        invoice_done_amount = self.money_invoice_id.invoice_done_amount + self.invoice_subtotal
        to_invoice_amount = self.money_invoice_id.amount - invoice_done_amount

        self.money_invoice_id.write({
            'invoice_done_amount': invoice_done_amount,
            'to_invoice_amount': to_invoice_amount,
            'date': self.invoice_date,
        })
        self._line_qty_write()
        remain_quantity = 0
        #判断发票是否已开完
        if self.invoice_type == 'in':
            for line in self.money_invoice_id.move_id.line_in_ids:
                remain_quantity += (line.goods_qty - line.quantity_invoice_done)
        else:
            for line in self.money_invoice_id.move_id.line_out_ids:
                remain_quantity += (line.goods_qty - line.quantity_invoice_done)

        if remain_quantity != 0:
            #如果发票未开完，生成新的草稿
            return self.money_invoice_id._generate_tax_invoice()
        else:
            if self.money_invoice_id.state != 'done': 
                self.money_invoice_id.money_invoice_done()

    def _line_qty_write(self):
        if self.money_invoice_id:
            for line in self.line_ids:
                remain_quantity = line.wh_move_line_id.goods_qty - line.wh_move_line_id.quantity_invoice_done
                if line.quantity == 0:
                    quantity_invoice_done = line.wh_move_line_id.quantity_invoice_done
                    line.wh_move_line_id.write({'quantity_invoice_done': quantity_invoice_done})                    
                    line.unlink()
                else:
                    if line.quantity > remain_quantity:
                        raise UserError(u'%s的开票数量大于发货数量' %(line.goods_name))
                    quantity_invoice_done = line.wh_move_line_id.quantity_invoice_done + line.quantity
                    line.wh_move_line_id.write({'quantity_invoice_done': quantity_invoice_done})

# 定义发票明细行
class tax_invoice_line(models.Model):
    _name = 'tax.invoice.line'
    _description = u'发票明细'
    @api.one
    @api.depends('quantity')
    def _onchange_quantity(self):
        '''当订单行的商品数量变化时，更新金额和税额'''
        self.amount = self.price * self.quantity
        self.tax = self.amount/(100 + self.tax_rate) * self.tax_rate

    wh_move_line_id = fields.Many2one('wh.move.line', u'出入库单行',
                                   ondelete='cascade',
                                   help=u'对应的出入库单行')
    tax_invoice_id = fields.Many2one('tax.invoice', u'发票', help=u'关联发票')
    goods_id = fields.Many2one('goods', string=u'商品', required=True,
                               index=True, ondelete='restrict',
                               help=u'该单据行对应的商品', readonly=True)
    goods_name = fields.Char(u'货物名称', readonly=True)
    partnumber = fields.Char(u'规格型号', readonly=True)
    uom = fields.Char(u'单位', readonly=True)
    quantity = fields.Float(u'数量')
    price = fields.Float(u'价格', readonly=True)
    tax_rate = fields.Float(u'税率', readonly=True)

    amount = fields.Float(u'金额', readonly=True,
                          compute=_onchange_quantity,
                            store=True,
                            digits=dp.get_precision('Amount'),
                            help=u'含税单价 乘以 数量')    
    tax = fields.Float(u'税额', readonly=True,
                              compute=_onchange_quantity,
                              store=True,
                              digits=dp.get_precision('Amount'),
                              help=u'税额')
    tax_catagory = fields.Char(
        u'税收分类编码', help=u'20170101以后使用的税收分类编码，这个很重要', readonly=True)

class WhMoveLine(models.Model):
    _inherit = 'wh.move.line'

    quantity_invoice_done = fields.Float(u'已开发票数量', readonly=True)