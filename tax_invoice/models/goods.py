# -*- coding: utf-8 -*-

from odoo import models, fields

class goods(models.Model):
    """
    继承了core里面定义的goods 模块，增加税收分类编码，并定义了视图和添加字段。
    """
    _inherit = "goods"

    tax_catagory = fields.Char(u'发票税收分类编码')
