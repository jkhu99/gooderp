# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2020  永远的不知().
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundaption, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from odoo import models, fields, api
from odoo.exceptions import UserError
import odoo.addons.decimal_precision as dp
# 状态可选值
TAX_INVOICE_STATES = [
    ('draft', u'待开具'),
    ('submit', u'已通知开票'),
    ('done', u'已完成')]
# 发票明细行
class TaxInvoiceTrack(models.TransientModel):
    _name = 'tax.invoice.track'
    _description = u'发票明细跟踪'
    _rec_name = 'money_invoice_id'

    invoice_state = fields.Selection(TAX_INVOICE_STATES, u'状态', readonly=True,
                             help=u"发票状态标识，新建时状态为草稿，确认后状态为完成", index=True,
                             copy=False, default='draft')
                                                          
    partner_id = fields.Many2one('partner', u'业务伙伴', 
                                 help=u'该单据对应的业务伙伴')

    order = fields.Char(u'订单号', readonly=True)
    name = fields.Many2one('tax.invoice.line', string=u'发票行',
                           help=u'待核销行对应的发票行')
    date = fields.Date(string=u'结算单日期', required=True,
                       default=lambda self: fields.Date.context_today(self),
                       help=u'结算单日期')
    tax_invoice_id = fields.Many2one('tax.invoice', u'发票号', help=u'关联发票')
    money_invoice_id = fields.Many2one('money.invoice', u'结算单', help=u'关联发票')
    
    invoice_number = fields.Char(u'发票号码', help=u'发票号码，多张发票中间用空格分开')
    
    quantity = fields.Float(string=u'数量',
                            digits=dp.get_precision('Quantity'),
                            help=u'待核销行对应数量')

    partner_order = fields.Char(u'外部单号', readonly=True)

    goods_id = fields.Many2one('goods', string=u'商品', required=True,
                               help=u'该单据行对应的商品', readonly=True)

    uom = fields.Char(u'单位', readonly=True)

    cost_unit = fields.Float(u'成本', readonly=True, digits=dp.get_precision('Price'))
    price = fields.Float(u'价格', readonly=True, digits=dp.get_precision('Price'))
    tax_rate = fields.Float(u'税率', readonly=True)
    amount = fields.Float(u'金额', readonly=True,
                          store=True,
                          digits=dp.get_precision('Amount'),
                          help=u'含税单价 乘以 数量')
    amount_payed = fields.Float(u'结算单已付款金额', readonly=True,
                          store=True,
                          digits=dp.get_precision('Amount'),
                          help=u'已付款金额')                          
