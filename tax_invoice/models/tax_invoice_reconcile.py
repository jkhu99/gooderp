# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2020  永远的不知().
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundaption, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from odoo import models, fields, api
from odoo.exceptions import UserError
import odoo.addons.decimal_precision as dp

TAX_INVOICE_STATES = [
    ('draft', u'草稿'),
    ('done', u'已完成')]


class TaxInvoiceReconcile(models.Model):
    _name = 'tax.invoice.reconcile'
    _description = u'发票对账'
    _rec_name = 'invoice_number'
    _order = 'invoice_date DESC'

    state = fields.Selection(TAX_INVOICE_STATES, u'状态', readonly=True,
                             help=u"发票状态标识，新建时状态为草稿，确认后状态为完成", index=True,
                             copy=False, default='draft')
    my_company_name = fields.Char(u'本公司名称', help=u'本公司名称', readonly=True,
                                  default=lambda self: self.env['res.company']._company_default_get().name)
    my_company_code = fields.Char(u'纳税人税号', readonly=True,
                                  default=lambda self: self.env['res.company']._company_default_get().vat)
    my_company_address = fields.Char(u'地址、电话', readonly=True,
                                     default=lambda self: self.env['res.company']._company_default_get().company_registry)
    my_company_bank_number = fields.Char(u'开户行及帐号', readonly=True,
                                         default=lambda self: (self.env['res.company']._company_default_get().bank_account_id.name
                                                               + self.env['res.company']._company_default_get().bank_account_id.num))

    partner_id = fields.Many2one('partner', u'业务伙伴', ondelete='restrict',
                                 help=u'该单据对应的业务伙伴')
    partner_code = fields.Char(u'纳税人税号', readonly=True)
    partner_address = fields.Char(u'地址、电话', readonly=True)
    partner_bank_number = fields.Char(u'开户行及帐号', readonly=True)

    invoice_type = fields.Selection([('in', u'进项发票'),
                                     ('out', u'销项发票')], u'发票种类')

    invoice_code = fields.Char(u'发票代码', help=u'该单据对应的发票代码')
    invoice_number = fields.Char(u'发票号码', help=u'发票号码，多张发票中间用空格分开')

    @api.multi
    @api.depends('line_ids.quantity')
    def _compute_to_amount(self):
        '''当行的发票明细的商品数量变化时，计算对账单的金额'''
        invoice_amount = 0
        invoice_subtotal = 0
        for line in self.line_ids:
            invoice_subtotal += (line.quantity * line.price)
            if line.tax_rate:
                invoice_amount += ((line.quantity * line.price) /
                                   (1 + line.tax_rate * 0.01))
        self.invoice_amount = invoice_amount
        self.invoice_tax = invoice_subtotal - invoice_amount
        self.invoice_subtotal = invoice_subtotal

    invoice_amount = fields.Float(u'合计金额',
                                  store=True, compute=_compute_to_amount,
                                  digits=dp.get_precision('Amount'))
    invoice_tax = fields.Float(u'合计税额',
                               store=True, compute=_compute_to_amount,
                               digits=dp.get_precision('Amount'))
    invoice_subtotal = fields.Float(u'价税合计',
                                    store=True, compute=_compute_to_amount,
                                    digits=dp.get_precision('Amount'))
    start_date = fields.Date(u'核销发票起始日期')
    invoice_date = fields.Date(u'开票日期')

    line_ids = fields.One2many('tax.invoice.reconcile.line', 'tax_id',
                               string=u'发票明细行', readonly=True,
                               states={'draft': [('readonly', False)]},
                               help=u'发票明细行')
    source_ids = fields.One2many('source.invoice.line', 'tax_id',
                                 string=u'待核销行', readonly=True,
                                 states={'draft': [('readonly', False)]},
                                 help=u'发票待核销行')

    _sql_constraints = [
        ('unique_invoice_code_number',
         'unique( invoice_code, invoice_number)',
         u'发票代码+发票号码不能相同!'),
    ]

    def write(self, vals):
        if vals.get('invoice_number'):
            for number in vals.get('invoice_number').split(' '):
                if len(number) != 8:
                    raise UserError(u'发票号码 %s 长度为 %s ,应该为8' %
                                    (number, len(number)))
        if (vals.get('state') == 'done' or self.state == 'done'):
            if (not vals.get('invoice_number')) and (not self.invoice_number):
                raise UserError(u'发票号码不能为空')

            if not vals.get('invoice_date') and (not self.invoice_date):
                raise UserError(u'开票日期不能为空')
        super(TaxInvoiceReconcile, self).write(vals)
        return True

    def tax_invoice_reconcile_draft(self):
        '''撤销确认'''
        self.write({
            'state': 'draft',
        })

    def tax_invoice_reconcile_done(self):
        '''审核时不合法的给出报错'''
        self.ensure_one()
        if self.state == 'done':
            raise UserError(u'请不要重复确认！')
        for line in self.line_ids:
            if line.to_reconcile != 0:
                raise UserError(u'发票行的商品%s数量尚未核销完' % (self.goods_id.name))
        to_tax_invoice_ids = []
        to_tax_invoice_line_ids = []
        for line in self.source_ids:
            if line.this_reconcile != 0:
                line.name.quantity = line.this_reconcile
                to_tax_invoice_line_ids.append(line.name)
                if line.tax_invoice_id not in to_tax_invoice_ids:
                    to_tax_invoice_ids.append(line.tax_invoice_id)
                # if to_tax_invoice_dict[line.tax_invoice_id]:
                #     to_tax_invoice_dict[line.tax_invoice_id].append(line.name)
                # else:
                #     to_tax_invoice_dict[line.tax_invoice_id] = [line.name]
            else:
                line.unlink()
        for invoice in to_tax_invoice_ids:
            for line in invoice.line_ids:
                if line not in to_tax_invoice_line_ids:
                    print(line.goods_name)
                    line.quantity = 0
            invoice.write(
                {'invoice_number': self.invoice_number,
                 'invoice_date': self.invoice_date,
                 'invoice_code': self.invoice_code,
                 })
            invoice.tax_invoice_done()
        self.write({
            'state': 'done',
        })

    # @api.onchange('partner_id', 'invoice_type')
    def loadsource_ids(self):
        """
        onchange 类型字段 当改变 客户业务类型 自动生成 对应的
        核销单各种明细。
        :return:
        """
        if not self.partner_id or not self.invoice_type or not self.start_date or not self.invoice_date or not self.line_ids:
            raise UserError(u'合作伙伴、核销起始日期，开票日期、开票明细不能为空')
        self.partner_code = self.partner_id.tax_num
        self.partner_address = self.partner_id.main_address
        self.partner_bank_number = self.partner_id.bank_name + self.partner_id.bank_num
        self.source_ids.unlink()
        self.source_ids = self._get_tax_invoice_line()

    @api.multi
    def _get_tax_invoice_line(self):
        """
        搜索到满足条件的yscinvoiceLine记录并且取出invoice对象 构造出one2many的
        :return:
        """
        TaxInvoices = self.env['tax.invoice'].search([
            ('money_invoice_id.date', '>=', self.start_date),
            ('money_invoice_id.date', '<=', self.invoice_date),
            ('invoice_type', '=', self.invoice_type),
            ('partner_id', '=', self.partner_id.id),
            ('state', '!=', 'done')])

        result = []
        for TaxInvoice in TaxInvoices:
            if TaxInvoice.invoice_type == 'in':
                order = TaxInvoice.buy_order_id
            else:
                order = TaxInvoice.sell_order_id
            for line in TaxInvoice.line_ids:
                for invoice in self.line_ids:
                    if line.goods_id == invoice.goods_id:
                        result.append({
                            'name': line.id,
                            'tax_invoice_id': TaxInvoice.id,
                            'order': order.name,
                            'date': TaxInvoice.money_invoice_id.date,
                            'partner_order': TaxInvoice.partner_order,
                            'goods_id': line.goods_id.id,
                            'uom': line.uom,
                            'tax_rate': line.tax_rate,
                            'price': line.price,
                            'amount': line.amount,
                            'quantity': line.quantity,
                            'reconciled': line.wh_move_line_id.quantity_invoice_done,
                            'this_reconcile': 0,
                            'amount_payed': TaxInvoice.money_invoice_id.reconciled,
                        })
        return result


# 待核销发票明细行
class SourceInvoiceLine(models.Model):
    _name = 'source.invoice.line'
    _description = u'待核销行'
    _rec_name = 'order'

    tax_id = fields.Many2one('tax.invoice.reconcile', string=u'收付款单',
                             ondelete='cascade',
                             help=u'待核销行对应的发票单')  # 收付款单上的待核销行

    order = fields.Char(u'订单号', readonly=True)
    name = fields.Many2one('tax.invoice.line', string=u'发票行',
                           ondelete='cascade',
                           help=u'待核销行对应的发票行')
    date = fields.Date(string=u'结算单日期', required=True,
                       default=lambda self: fields.Date.context_today(self),
                       help=u'结算单日期')
    tax_invoice_id = fields.Many2one('tax.invoice', u'发票', help=u'关联发票')
    quantity = fields.Float(string=u'数量',
                            digits=dp.get_precision('Quantity'),
                            help=u'待核销行对应数量')
    reconciled = fields.Float(string=u'已核销数量',
                              digits=dp.get_precision('Quantity'),
                              help=u'待核销行已核销掉的数量')

    this_reconcile = fields.Float(string=u'本次核销数量',
                                  digits=dp.get_precision('Quantity'),
                                  help=u'本次要核销掉的数量')
    partner_order = fields.Char(u'外部单号', readonly=True)

    goods_id = fields.Many2one('goods', string=u'商品', required=True,
                               index=True, ondelete='restrict',
                               help=u'该单据行对应的商品', readonly=True)

    uom = fields.Char(u'单位', readonly=True)

    price = fields.Float(u'价格', readonly=True,
                         digits=dp.get_precision('Price'))
    tax_rate = fields.Float(u'税率', readonly=True)
    amount = fields.Float(u'金额', readonly=True,
                          store=True,
                          digits=dp.get_precision('Amount'),
                          help=u'含税单价 乘以 数量')
    amount_payed = fields.Float(u'已付款', readonly=True,
                                store=True,
                                digits=dp.get_precision('Amount'),
                                help=u'结算单已付款金额')

    @api.one
    def line_all_reconcile(self):
        """
        核销本行未核销
        :return:
        """
        if self.tax_id.state != 'draft':
            raise ValueError(u'已确认的单据不能执行这个操作')
        tax_line_id = False
        for line in self.tax_id.line_ids:
            if self.goods_id == line.goods_id:
                tax_line_id = line
                this_reconcile = self.this_reconcile
                if line.to_reconcile >= (self.quantity - this_reconcile):
                    this_reconcile = self.quantity
                else:
                    this_reconcile += line.to_reconcile
                self.write({
                    'this_reconcile': this_reconcile
                })
                break
        if tax_line_id:
            to_reconcile = tax_line_id.quantity
            for line in self.tax_id.source_ids:
                if self.goods_id == line.goods_id:
                    to_reconcile -= line.this_reconcile
            if to_reconcile >= 0:
                tax_line_id.to_reconcile = to_reconcile
            else:
                raise UserError(u'发票行的商品%s数量不够核销' % (self.goods_id.name))
        else:
            raise UserError(u'发票明细行无此商品%s' % (self.goods_id.name))


# 发票明细行
class TaxInvoiceReconcileLine(models.Model):
    _name = 'tax.invoice.reconcile.line'
    _description = u'发票明细'
    tax_id = fields.Many2one('tax.invoice.reconcile', string=u'发票',
                             ondelete='cascade',
                             help=u'待核销行对应的发票单')  # 收付款单上的待核销行
    goods_id = fields.Many2one('goods', string=u'商品', required=True,
                               index=True, ondelete='restrict',
                               help=u'该单据行对应的商品')
    uom = fields.Char(u'单位')

    price = fields.Float(u'含税价格', digits=dp.get_precision('Price'))
    tax_rate = fields.Float(u'税率')
    quantity = fields.Float(string=u'数量',
                            digits=dp.get_precision('Quantity'),
                            help=u'待核销行对应数量')

    @api.multi
    @api.depends('tax_id.source_ids.this_reconcile')
    def _compute_to_reconcile(self):
        '''当行的商品数量变化时，计算未核销数量'''
        for line in self:
            to_reconcile = line.quantity
            for source_line in line.tax_id.source_ids:
                if source_line.goods_id == line.goods_id:
                    if source_line.this_reconcile != 0:
                        to_reconcile -= source_line.this_reconcile
            if to_reconcile >= 0:
                line.to_reconcile = to_reconcile
            else:
                raise UserError(u'发票行的商品%s数量不够核销' % (line.goods_id.name))

    to_reconcile = fields.Float(string=u'未核销数量',
                                store=True, compute=_compute_to_reconcile,
                                digits=dp.get_precision('Quantity'),
                                help=u'待核销行未核销掉的数量')

    @api.multi
    @api.onchange('goods_id')
    def onchange_goods_id(self):
        '''当行的商品变化时，带出商品上的单位、价格、税率'''
        if self.goods_id:
            self.uom = self.goods_id.uom_id.name
            self.tax_rate = self.goods_id.get_tax_rate(
                self.goods_id, self.tax_id.partner_id, 'sell')
            quotation_line_id = self.env['sell.quotation.line'].search([('goods_id', '=', self.goods_id.id),
                                                                        ('partner_id', '=',
                                                                         self.tax_id.partner_id.id),
                                                                        ('state',
                                                                         '=', 'done'),
                                                                        ('qty', '<=', self.quantity)],
                                                                       order='date desc')

            if not quotation_line_id:
                raise UserError(u'客户%s商品%s不存在已确认的起订量低于%s的报价单！' % (
                    self.tax_id.partner_id.name, self.goods_id.name, self.quantity))

            self.price = quotation_line_id[0].price

    @api.multi
    @api.onchange('quantity')
    def onchange_quantity(self):
        '''当行的商品数量变化时'''
        self.to_reconcile = self.quantity
