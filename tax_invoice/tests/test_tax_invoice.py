# -*- coding: utf-8 -*-
from odoo.tests.common import TransactionCase
from odoo.exceptions import UserError


class TestTaxInvoice(TransactionCase):

    def setUp(self):
        '''准备基本数据'''
        super(TestTaxInvoice, self).setUp()
        self.env.ref('core.yixun').credit_limit = 10000000
        self.env.ref('core.goods_category_1').account_id = self.env.ref(
            'finance.account_goods').id
        self.env.ref('warehouse.wh_in_whin0').date = '2020-03-06'
        self.bank_account = self.env['bank.account'].create({'name': '浦发银行',
                                                             'num': '123456789987654321'
                                                             })
        self.sellorder = self.env.ref('tax_invoice.sell_order_1')
        self.sellorder.sell_order_done()

        self.delivery = self.env['sell.delivery'].search(
            [('order_id', '=', self.sellorder.id)])
        self.my_company = self.delivery.create_uid.company_id

        self.buyorder = self.env.ref('tax_invoice.buy_order_1')
        self.buyorder.bank_account_id = False
        self.buyorder.buy_order_done()
        self.receipt = self.env['buy.receipt'].search(
            [('order_id', '=', self.buyorder.id)])

        self.goods = self.env.ref('goods.computer')
        '''测试采购发票'''
        # 测试银行账号不能为空
        self.bank_account.write({
            'num': '123456789987654321'
        })
        self.my_company.write({
            'vat': '123456789987654321',
            'company_registry': '上海市浦东新区张江高科一号院',
            'bank_account_id': self.bank_account
        })
        # 通过确认receipt创建采购发票
        warehouse_id = self.env['warehouse'].search(
            [])       
        self.receipt.buy_receipt_done()
        self.goods.compute_stock_qty()

        tax_invoice = self.env['tax.invoice'].search(
            [('name', '=', self.receipt.name)])
        self.assertEqual(tax_invoice.state, 'draft')
        self.assertEqual(tax_invoice.invoice_type, 'in')
        # 发票行不能为空
        self.assertTrue(tax_invoice.line_ids)

    def test_sell_tax_invoice(self):
        '''测试销售发票'''
        self.my_company.write({
            'vat': None,
            'company_registry': '上海市浦东新区张江高科一号院',
            'bank_account_id': self.bank_account
        })
        with self.assertRaises(UserError):
            self.delivery.sell_delivery_done()
        # 测试注册地不能为空
        self.my_company.write({
            'vat': '123456789987654321',
            'company_registry': None,
            'bank_account_id': self.bank_account
        })
        with self.assertRaises(UserError):
            self.delivery.sell_delivery_done()

        # 测试银行账号不能为空
        self.bank_account.write({
            'num': '123456789987654321'
        })
        self.my_company.write({
            'vat': '123456789987654321',
            'company_registry': '上海市浦东新区张江高科一号院',
            'bank_account_id': self.bank_account
        })
        with self.assertRaises(UserError):
            self.delivery.sell_delivery_done()

        # 通过确认delivery创建销售发票
        self.delivery.sell_delivery_done()

        invoice = self.env['money.invoice'].search(
            [('name', '=', self.delivery.name)])

        tax_invoice = self.env['tax.invoice'].search(
            [('name', '=', self.delivery.name)])
        # 公司的 draft_invoice参数设置，创建的结算单应为草稿
        self.assertEqual(invoice.state, 'draft')

        # 创建时发票的状态应该草稿
        self.assertEqual(tax_invoice.state, 'draft')
        self.assertEqual(tax_invoice.invoice_type, 'out')

        # 发票行不能为空
        self.assertTrue(tax_invoice.line_ids)

        # 测试申请开票
        tax_invoice.tax_invoice_submit()
        self.assertEqual(tax_invoice.state, 'submit')

        # 重复申请
        with self.assertRaises(UserError):
            tax_invoice.tax_invoice_submit()

        # 检查发票号码不能为空
        vals = {
            'state': 'done',
            'invoice_date': '2020-03-06',
        }
        with self.assertRaises(UserError):
            tax_invoice.write(vals)
        # 检查发票号码应为8位
        vals = {
            'state': 'done',
            'invoice_date': '2020-03-06',
            'invoice_number': '2020030',
        }
        with self.assertRaises(UserError):
            tax_invoice.write(vals)
        # 检查开票日期不能为空
        vals = {
            'state': 'done',
            'invoice_number': '20200306',
        }
        with self.assertRaises(UserError):
            tax_invoice.write(vals)

        # 写入发票号码 开票日期
        vals = {
            'invoice_date': '2020-03-06',
            'invoice_number': '20200306',
        }
        tax_invoice.write(vals)

        tax_invoice.tax_invoice_done()
        self.assertEqual(tax_invoice.state, 'done')

        # 重复done
        with self.assertRaises(UserError):
            tax_invoice.tax_invoice_done()

        # 检查结算单的状态 发票号 发票日期
        self.assertEqual(invoice.state, 'done')
        self.assertEqual(invoice.bill_number, tax_invoice.invoice_number)
        self.assertEqual(invoice.invoice_date, tax_invoice.invoice_date)

    # def test_sell_tax_invoice(self):
    #     '''测试发票异常'''
    #     self.my_company.write({
    #         'vat': None,
    #         'company_registry': '上海市浦东新区张江高科一号院',
    #         'bank_account_id': self.bank_account
    #     })
    #     with self.assertRaises(UserError):
    #         self.receipt.buy_receipt_done()
    #     # 测试注册地不能为空
    #     self.my_company.write({
    #         'vat': '123456789987654321',
    #         'company_registry': None,
    #         'bank_account_id': self.bank_account
    #     })
    #     with self.assertRaises(UserError):
    #         self.receipt.buy_receipt_done()

    #     # 测试银行账号不能为空
    #     self.bank_account.write({
    #         'num': '123456789987654321'
    #     })
    #     self.my_company.write({
    #         'vat': '123456789987654321',
    #         'company_registry': '上海市浦东新区张江高科一号院',
    #         'bank_account_id': self.bank_account
    #     })
    #     with self.assertRaises(UserError):
    #         self.receipt.buy_receipt_done()
