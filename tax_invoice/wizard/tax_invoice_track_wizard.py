# -*- coding: utf-8 -*-

from datetime import date
from odoo import models, fields, api
from odoo.exceptions import UserError
import datetime

class TaxInvoiceTrackWizard(models.TransientModel):
    _name = 'tax.invoice.track.wizard'
    _description = u'发票跟踪表向导'

    @api.model
    def _default_date_start(self):
        return date.today()-datetime.timedelta(days=30)

    @api.model
    def _default_date_end(self):
        return date.today()

    date_start = fields.Date(u'开始日期', default=_default_date_start,
                             help=u'报表汇总的开始日期，默认为公司启用日期')
    date_end = fields.Date(u'结束日期', default=_default_date_end,
                           help=u'报表汇总的结束日期，默认为当前日期')
    partner_id = fields.Many2one('partner', u'客户', 
                                 help=u'只统计选定的客户')
    invoice_type = fields.Selection([('in', u'进项发票'),
                                     ('out', u'销项发票')], u'发票种类', default='out')

    def _get_domain(self):
        '''返回wizard界面上条件'''
        domain = [
            ('money_invoice_id.date', '>=', self.date_start),
            ('money_invoice_id.date', '<=', self.date_end),
            ('invoice_type', '=', self.invoice_type)
        ]
        if self.user_has_groups('sell.own_data_groups'):
            self.user_id = self.env.user

        if not self.user_has_groups('sell.group_sell'):
            raise UserError(u'错误的操作')
        if self.partner_id:
            domain.append(('partner_id', '=', self.partner_id.id))
        return domain

    def _prepare_track_line(self, line, tax_invoice, order):
        '''返回跟踪表明细行（非小计行）'''

        result = ({
            'partner_id':tax_invoice.partner_id.id,
            'tax_invoice_id': tax_invoice.id,
            'money_invoice_id': tax_invoice.money_invoice_id.id,
            'invoice_number': tax_invoice.invoice_number,
            'order': order.name,
            'date': tax_invoice.money_invoice_id.date,
            'partner_order': tax_invoice.partner_order,
            'goods_id': line.goods_id.id,
            'uom': line.uom,
            'tax_rate': line.tax_rate,
            'price': line.price,
            'cost_unit': line.wh_move_line_id.cost_unit,            
            'amount': line.amount,
            'quantity': line.quantity,
            'amount_payed': tax_invoice.money_invoice_id.reconciled,
            'invoice_state': tax_invoice.state
        })
        return result

    @api.multi
    def button_ok(self):
        self.ensure_one()
        res = []
        if self.date_end < self.date_start:
            raise UserError(u'开始日期不能大于结束日期！\n所选开始日期:%s 所选结束日期:%s' %
                            (self.date_start, self.date_end))

        tax_invoice_track_ids = self.env['tax.invoice.track'].search([('quantity', '>', 0)])
        for track in tax_invoice_track_ids:
            track.state = "draft"
            track.unlink()

        tax_invoice_ids = self.env['tax.invoice'].search(self._get_domain())
        for tax_invoice in tax_invoice_ids:
            if tax_invoice.invoice_type == 'in':
                order = tax_invoice.buy_order_id
            else:
                order = tax_invoice.sell_order_id
            for line in tax_invoice.line_ids:
                # 创建跟踪表明细行（非小计行）
                track = self.env['tax.invoice.track'].create(
                    self._prepare_track_line(line, tax_invoice, order))
                res.append(track.id)

        action = self.env.ref('tax_invoice.tax_invoice_track_action').read()[0]
        return action
        return {
            'name': u'发票跟踪表',
            'view_type': 'form',
            'view_mode': 'tree',
            'view_id': False,
            'views': [(view.id, 'tree')],
            'res_model': 'tax.invoice.track',
            'type': 'ir.actions.act_window',
            'domain': [('id', 'in', res)],
            'limit': 65535,
        }
