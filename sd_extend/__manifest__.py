# -*- coding: utf-8 -*-
# Copyright 2018 上海开阖软件 ((http://www.osbzr.com).)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    'name': 'SD扩展',
    'version': '10.0.0.1',
    'author': '永远的不知',
    'maintainer': 'False',
    'website': '',
    'category': 'gooderp',
    'summary': '扩展gooderp的细节，适合sd公司使用',
    'description': """芯片行业需求""",
    'depends': [
        'goods', 'buy', 'sell', 'sell_quotation',
    ],
    # always loaded
    'data': [
        'views/goods_view.xml',
        'views/production_view.xml',
        'views/money_view.xml',
        'views/buy_view.xml',
        'views/sell_view.xml',
        'views/sell_quotation_view.xml',
        'views/import_sell_order_wizard_view.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
    ],
    'installable': True,
    'application': False,
}
