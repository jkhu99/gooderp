# -*- coding: utf-8 -*-

from odoo import fields, models, api
import odoo.addons.decimal_precision as dp
import time
from datetime import datetime, timedelta
from odoo.exceptions import UserError
import xlrd
import base64


class importsellorderwizard(models.TransientModel):
    _name = 'import.sell.order.wizard'
    _description = 'import sell order'

    file = fields.Binary(u'导入文件',)

    def import_sell_order(self):
        """
        通过Excel文件导入信息创建订单
        """
        if not self.user_has_groups('sell.sell_discount_groups'):
            raise UserError(u'错误的操作')
        xls_data = xlrd.open_workbook(
            file_contents=base64.decodestring(self.file))
        table = xls_data.sheets()[0]
        colum_names = table.row_values(0)
        data = []
        print(type(colum_names))
        # 数据读入，导入成字典格式
        for row in range(1, table.nrows):
            if table.row_values(row):
                dict = {}
                for i in range(len(colum_names)):
                    dict[colum_names[i]] = table.row_values(row)[i]
                data.append(dict)
        # 数据处理
        in_xls_data = {}
        ordernum = ''
        vals = {}
        order_lines = []
        for dict in data:
            ref = dict[u'订单号'] or ''
            if(ordernum == '') and (ref == ''):
                raise UserError(u'首行订单的订单号u不能为空')
            order_id = self.env['sell.order'].search([
                ('ref', '=', ref)])               
            if order_id:
                raise UserError(u'已存在客户订单号为%s的订单，请勿重复导入'% ref)           
            if(ordernum != ref) and (ref != ''):
                if order_lines:
                    order_id = self.env['sell.order'].create(vals)
                    order_id.write({'line_ids': [
                        (0, 0, line) for line in order_lines]})
                    order_lines = []
                ordernum = ref
                partner_id = self.env['partner'].search([
                    ('name', '=', dict[u'公司名称'])])                      
                if partner_id:           
        # warehouse = (self.env.ref("warehouse.warehouse_lichuang"))
                    vals = {
                        'type': 'sell',
                        'partner_id': partner_id.id,
                        'ref': ref,
                        'delivery_date': self.date_format(dict[u'需求日期']),
                        'date': self.date_format(dict[u'订单日期']),
                        'user_id': partner_id.responsible_id.id or self.user.id,
                        'warehouse_id': self.env['warehouse'].search([('type', '=', 'stock')], limit=1).id,
                }    
                else:
                    raise UserError(u'不存在名称为%s的客户，请检查名称是否正确或新建客户'% dict[u'公司名称'])                               
                order_lines = []
                goods_id = self.env['goods'].search([
                    ('name', '=', dict[u'规格型号'])])   
                if goods_id:         
                    order_lines.append({
                        'goods_id': goods_id.id,
                        'uom_id' : goods_id.uom_id.id,
                        'quantity': int(dict[u'需求数量']),
                        'price_taxed': float(dict[u'单价']),
                        'tax_rate': float(dict[u'税率']),
                        'subtotal': float(dict[u'总金额']),
                    })        
                else:
                    raise UserError(u'不存在名称为%s的商品，请检查名称是否正确或新建商品'% dict[u'规格型号'])    
            else:
                goods_id = self.env['goods'].search([
                    ('name', '=', dict[u'规格型号'])])                 
                order_lines.append({
                    'goods_id': goods_id.id,
                    'uom_id' : goods_id.uom_id.id,
                    'quantity': int(dict[u'需求数量']),
                    'price_taxed': float(dict[u'单价']),
                    'tax_rate': float(dict[u'税率']),
                    'subtotal': float(dict[u'总金额']),
                })   
        #前面流程最后一个订单不会创建订单，需在这里创建
        if order_lines:
            order_id = self.env['sell.order'].create(vals)
            order_id.write({'line_ids': [
                (0, 0, line) for line in order_lines]})
            order_lines = []                

        view = self.env.ref('sell.sell_order_tree')
        #跳转到销售订单窗口
        return {
            'name': (u'销货订单'),           
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'sell.order',
            'views': [(view.id, 'tree')],
            'view_id': 'view.id',
            'context': self.env.context,
        }

    def date_format(self, data):
        # 字符串日期格式化成odoo日期
        if type(data) in (int, float):
            data = str(data)
            data = list(data[0:8]) # 转化
            data.insert(4,'-') 
            data.insert(7, '-')
            data = ''.join(data) # 转化回来
            py_date = data            
        else:
            py_date = data
        return py_date
