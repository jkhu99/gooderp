# -*- coding: utf-8 -*-

from odoo import fields, models, api
import odoo.addons.decimal_precision as dp
from odoo.tools import float_compare, float_is_zero

class BuyOrder(models.Model):
    _inherit = "buy.order"

    amount_upper = fields.Char(u'金额总计(大写)', compute='_compute_amount_upper', store=True,
                               help=u'大写金额')

    @api.one
    @api.depends('amount')
    def _compute_amount_upper(self):
        # 计算大写总金额
        self.amount_upper = self.env['res.currency'].rmb_upper(self.amount)

    @api.multi
    def print_buy_order(self):
        # 根据不同供应商打印不同的格式
        if self.partner_id.name == u'上海芯旺微电子技术有限公司':
            action = self.env.ref('sd_extend.report_buy_order_chipon').read()[0]
        else:
            action = self.env.ref('buy.report_buy_order').read()[0]
        action['name'] = self.partner_id.name + u'_采购订单_' + self.name
        return action
    

    money_order_ids = fields.One2many('money.order', compute='_compute_money_order', string = u'付款单',
                                     readonly=True,
                                     help= u'订单对应的付款单')

    money_invoice_ids = fields.One2many('money.invoice', 'buy_order_id', string = u'结算单',
                                     readonly=True,
                                     help= u'订单对应的结算单')

    money_order_count = fields.Integer(
        compute='_compute_money_order', string=u'付款单数量', default=0)

    @api.depends('money_invoice_ids')
    def _compute_money_order(self):
        money_order_count = 0
        money_order_ids = []
        for order in self:
            for invoice in order.money_invoice_ids:
                for money_order in invoice.money_order_ids:
                    if money_order not in money_order_ids:
                        money_order_ids.append(money_order.id)
            order.money_order_ids = money_order_ids
            order.money_order_count = len(money_order_ids)

    @api.multi
    def action_view_money_order(self):
        '''
        This function returns an action that display existing picking orders of given purchase order ids.
        When only one found, show the picking immediately.
        '''
        self.ensure_one()
        # money_order_ids = self.money_order_ids
        money_order_ids = []
        for order in self:
            for invoice in order.money_invoice_ids:
                for money_order in invoice.money_order_ids:
                    if money_order not in money_order_ids:
                        money_order_ids.append(money_order.id)        
        # choose the view_mode accordingly
        action = self.env.ref(
            'money.money_receipt_action').read()[0]
        if len(money_order_ids) > 1:
            action['domain'] = "[('id','in',[" + \
                ','.join(map(str, money_order_ids)) + "])]"
        elif len(money_order_ids) == 1:
            view_id = self.env.ref('money.money_order_form').id
            action['views'] = [(view_id, 'form')]
            action['res_id'] = money_order_ids and money_order_ids[0] or False
        return action

class BuyOrderLine(models.Model):
    _inherit = 'buy.order.line'

    @api.onchange('subtotal')
    def _onchange_subtotal(self):
        '''当订单行的含税总价改变时，改变单价'''
        self.price_taxed = self.subtotal / self.quantity
        
    subtotal = fields.Float(inverse=_onchange_subtotal)

class BuyReceipt(models.Model):
    _inherit = "buy.receipt"
    def receipt_make_invoices_by_button(self):
        '''根据入库单生成当前合作伙伴丢失的结算单'''
        receipts = self.env['buy.receipt'].search([('partner_id', '=', self.partner_id.id)])
        for receipt in receipts:
            if receipt.state != 'done':
                continue            
            if receipt.invoice_id.name != False:
                continue
            if not receipt.invoice_by_receipt:
                continue            
            amount = receipt.amount
            tax_amount = sum(line.tax_amount for line in receipt.line_in_ids)
 
            categ = self.env.ref('money.core_category_purchase')
            if not float_is_zero(amount, 2):
                receipt.invoice_id = self.env['money.invoice'].create(
                    self._get_invoice_vals(
                        receipt.partner_id, categ, receipt.date, amount, tax_amount)
                )



