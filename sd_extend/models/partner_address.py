# -*- coding: utf-8 -*-
from odoo.exceptions import UserError
from odoo import models, fields, api

class PartnerAddress(models.Model):
    _inherit = ['partner.address']

    @api.multi
    def name_get(self):
        '''在many2one字段里显示 编号_名称'''
        res = []

        for adds in self:
            add_str = '%s%s%s%s%s %s %s' % (
                                      adds.province_id and adds.province_id.name or '',
                                      adds.city_id and adds.city_id.city_name or '',
                                      adds.county_id and adds.county_id.county_name or '',
                                      adds.town or '',
                                      adds.detail_address or '',
                                      adds.contact or '',
                                      adds.mobile or '',)
            res.append((adds.id, add_str))
        return res
