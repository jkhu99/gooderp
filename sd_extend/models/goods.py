# -*- coding: utf-8 -*-

from odoo import models, fields

class goods(models.Model):
    _inherit = "goods"

    package = fields.Char(u'封装')
    delivery_days = fields.Integer(u'货期（天）')    