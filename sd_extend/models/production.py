# -*- coding: utf-8 -*-

from odoo import models, fields, api
class WhAssembly(models.Model):
    _inherit = 'wh.assembly'
    _description = u'组装单'

    @api.multi
    def cancel_feeding(self):
        ''' 撤销发料 '''
        for order in self:
            if order.state == 'draft':
                raise UserError(u'请不要重复撤销发料')
            for line_out in order.line_out_ids:
                if line_out.state == 'done':
                    line_out.action_draft()
            for line in self.out_voucher_id.line_ids:
                line.state = 'draft'
                line.unlink()
            out_voucher_id = self.out_voucher_id
            self.out_voucher_id = None
            out_voucher_id.voucher_draft()  #反审核相关的凭证
            out_voucher_id.unlink()  # 删除相关的凭证
            order.state = 'draft'
            return       

    @api.onchange('lot')
    def onchange_lot(self):
        """
        组装单上的批次号改变时，将批次号更新到组合件
        """
        for line in self.line_in_ids:
            if line.goods_id.using_batch:
                line.lot = self.lot          

class outsource(models.Model):
    _inherit = 'outsource'
    _description = u'委外加工单'                

    @api.multi
    def cancel_feeding(self):
        ''' 撤销发料 '''
        for order in self:
            if order.state == 'draft':
                raise UserError(u'请不要重复撤销发料')
            for line_out in order.line_out_ids:
                if line_out.state == 'done':
                    line_out.action_draft()
            for line in self.out_voucher_id.line_ids:
                line.state = 'draft'
                line.unlink()
            out_voucher_id = self.out_voucher_id
            self.out_voucher_id = None
            out_voucher_id.voucher_draft()  #反审核相关的凭证
            out_voucher_id.unlink()  # 删除相关的凭证
            order.state = 'draft'
            return       

    @api.onchange('lot')
    def onchange_lot(self):
        """
        委外单上的批次号改变时，将批次号更新到组合件
        """
        for line in self.line_in_ids:
            if line.goods_id.using_batch:
                line.lot = self.lot                  

class WhDisassembly(models.Model):
    _inherit = 'wh.disassembly'
    _description = u'拆卸单'  

    @api.multi
    def cancel_feeding(self):
        ''' 撤销发料 '''
        for order in self:
            if order.state == 'draft':
                raise UserError(u'请不要重复撤销发料')
            for line_out in order.line_out_ids:
                if line_out.state == 'done':
                    line_out.action_draft()
            for line in self.out_voucher_id.line_ids:
                line.state = 'draft'
                line.unlink()
            out_voucher_id = self.out_voucher_id
            self.out_voucher_id = None
            out_voucher_id.voucher_draft()  #反审核相关的凭证
            out_voucher_id.unlink()  # 删除相关的凭证
            order.state = 'draft'
            return     

    @api.onchange('lot')
    def onchange_lot(self):
        """
        拆卸单上的批次号改变时，将批次号更新到子件
        """
        for line in self.line_out_ids:
            if line.goods_id.using_batch:
                line.lot = self.lot                                      