# -*- coding: utf-8 -*-

from odoo import fields, models, api
import odoo.addons.decimal_precision as dp

class SellOrder(models.Model):
    _inherit = "sell.order"

    amount_upper = fields.Char(u'金额总计(大写)', compute='_compute_amount_upper', store=True,
                               help=u'大写金额')                         

    @api.one
    @api.depends('amount')
    def _compute_amount_upper(self):
        # todo 实现明细行总金额
        self.amount_upper = self.env['res.currency'].rmb_upper(self.amount)


    money_order_ids = fields.One2many('money.order', compute='_compute_money_order', string = u'收款单',
                                     readonly=True,
                                     help= u'订单对应的收款单')

    money_invoice_ids = fields.One2many('money.invoice', 'sell_order_id', string = u'结算单',
                                     readonly=True,
                                     help= u'订单对应的结算单')

    money_order_count = fields.Integer(
        compute='_compute_money_order', string=u'收款单数量', default=0)

    @api.depends('money_invoice_ids')
    def _compute_money_order(self):
        money_order_count = 0
        money_order_ids = []
        for order in self:
            for invoice in order.money_invoice_ids:
                for money_order in invoice.money_order_ids:
                    if money_order not in money_order_ids:
                        money_order_ids.append(money_order.id)
            order.money_order_ids = money_order_ids
            order.money_order_count = len(money_order_ids)

    @api.multi
    def action_view_money_order(self):
        '''
        This function returns an action that display existing picking orders of given purchase order ids.
        When only one found, show the picking immediately.
        '''
        self.ensure_one()
        # money_order_ids = self.money_order_ids
        money_order_ids = []
        for order in self:
            for invoice in order.money_invoice_ids:
                for money_order in invoice.money_order_ids:
                    if money_order not in money_order_ids:
                        money_order_ids.append(money_order.id)        
        # choose the view_mode accordingly
        action = self.env.ref(
            'money.money_receipt_action').read()[0]
        if len(money_order_ids) > 1:
            action['domain'] = "[('id','in',[" + \
                ','.join(map(str, money_order_ids)) + "])]"
        elif len(money_order_ids) == 1:
            view_id = self.env.ref('money.money_order_form').id
            action['views'] = [(view_id, 'form')]
            action['res_id'] = money_order_ids and money_order_ids[0] or False
        return action


class SellDelivery(models.Model):
    _inherit = "sell.delivery"

    amount_upper = fields.Char(u'金额总计(大写)', compute='_compute_amount_upper', store=True,
                               help=u'大写金额')                         

    @api.one
    @api.depends('amount')
    def _compute_amount_upper(self):
        # todo 实现明细行总金额
        self.amount_upper = self.env['res.currency'].rmb_upper(self.amount)


    money_order_ids = fields.One2many('money.order', compute='_compute_money_order', string = u'发票号',
                                     readonly=True,
                                     help= u'订单对应的收款单')
    money_order_count = fields.Integer(
        compute='_compute_money_order', string=u'收款单数量', default=0)

    @api.depends('invoice_id')
    def _compute_money_order(self):
        for order in self:
            order.money_order_count = len(order.invoice_id.money_order_ids)
            order.money_order_ids += order.invoice_id.money_order_ids
