# -*- coding: utf-8 -*-

from odoo import fields, models, api

class MoneyInvoice(models.Model):
    _inherit = 'money.invoice'

    source_order_line_ids = fields.One2many('source.order.line', 'name', string=u'收付款单行',
                                      readonly=True,
                                      help=u'结算单对应的收付款单行')
    money_order_ids = fields.One2many('money.order', compute='_compute_money_order', string=u'收付款单',
                                      readonly=True,
                                      help=u'结算单对应的收付款单行')
    money_order_count = fields.Integer(
        compute='_compute_money_order', string=u'收付款单数量', readonly=True, default=0)
    @api.depends('source_order_line_ids')
    def _compute_money_order(self):
        for invoce in self:
            money_order_ids =[]
            for line in invoce.source_order_line_ids:
                if line.money_id not in money_order_ids:
                    money_order_ids.append(line.money_id.id)
            invoce.money_order_count = len(money_order_ids)
            invoce.money_order_ids = money_order_ids
        
            if invoce.category_id.type == 'income':
                invoce.sell_order_id._compute_money_order()
            else:
                invoce.buy_order_id._compute_money_order()

class SourceOrderLine(models.Model):
    _inherit = 'source.order.line'
    _description = u'待核销行'
    ref = fields.Char(string=u'外部订单号', readonly=True,
                      help=u'外部订单号，创建销售订单时建立')

    @api.one
    def line_all_reconcile(self):
        """
        核销本行未核销金额
        :return:
        """
        if self.money_id.state != 'draft':
            raise ValueError(u'已确认的单据不能执行这个操作')        
        if self.money_id.advance_payment >= (self.to_reconcile - self.this_reconcile):
            self.this_reconcile = self.to_reconcile
        else:
            self.this_reconcile += self.money_id.advance_payment
        self.money_id._compute_advance_payment()


class MoneyOrder(models.Model):
    _inherit = 'money.order'
    _description = u"收付款单"
    
    @api.multi
    def reload_source_ids(self):
        """
        调出合作伙伴未核销的结算单
        :return:
        """
        if not self.partner_id:
            return {}
        if self.state != 'draft':
            raise ValueError(u'已确认的单据不能执行这个操作')
        source_lines = []
        self.source_ids.unlink()
        for invoice in self.env['money.invoice'].search(self._get_invoice_search_list()):
            if not invoice.ref:
                invoice.ref = invoice.move_id.ref
            line = self._get_source_line(invoice)
            source_lines.append(line)
        self.source_ids = source_lines
        self._compute_advance_payment()

    @api.multi
    def reload_invoice_line(self):
        """
        刷新结算单信息
        :return:
        """
        if not self.partner_id:
            return {}

        invoice_search_list = [('partner_id', '=', self.partner_id.id),
                               ('state', '=', 'done')]
        if self.env.context.get('type') == 'get':
            invoice_search_list.append(('category_id.type', '=', 'income'))
        else:  # type = 'pay':
            invoice_search_list.append(('category_id.type', '=', 'expense'))

        for invoice in self.env['money.invoice'].search(invoice_search_list):
            if not  invoice.ref:
                invoice.ref = invoice.move_id.ref

        for line in self.source_ids:
            line.ref = line.name.ref
            line.date = line.name.date
            line.reconciled = line.name.reconciled
            line.amount = line.name.amount
            line.invoice_done_amount = line.name.invoice_done_amount
            line.to_reconcile = line.name.to_reconcile
            line.date_due = line.name.date_due
        
