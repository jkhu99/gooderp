# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import UserError
import odoo.addons.decimal_precision as dp


class SellQuotation(models.Model):
    _inherit = ['sell.quotation']
    _description = u'报价单'
    _order = 'date desc, id desc'    

    @api.onchange('partner_address_id')
    def onchange_partner_address_id(self):
        ''' 选择联系人，填充电话 '''
        if self.partner_address_id:
            self.contact = self.partner_address_id.contact
            self.mobile = self.partner_address_id.mobile
        else:
            if self.partner_id:
                return {'domain': {'partner_address_id': [('id', 'in', self.partner_id.child_ids)]}}                   

class SellQuotationLine(models.Model):
    _inherit = ['sell.quotation.line']

    no_tax_price = fields.Float(u'不含税价格', digits=dp.get_precision('Price'))
    tax_rate = fields.Float(u'税率(%)',
                            help=u'税率')    
    price = fields.Float(u'含税价格', digits=dp.get_precision('Price'))
    brand =  fields.Char(u'品牌')
 
    delivery_days = fields.Integer(u'货期（天）')   


    @api.multi
    @api.onchange('goods_id')
    def onchange_goods_id(self):
        ''' 当订单行的商品变化时，带出商品上的计量单位、含税价 '''
        if self.goods_id:
            self.uom_id = self.goods_id.uom_id
            # 报价单行单价取之前确认的报价单 #1795
            last_quo = self.search([('goods_id', '=', self.goods_id.id),
                                    ('partner_id', '=', self.quotation_id.partner_id.id),
                                    ('state', '=', 'done')], order='date desc', limit=1)
            self.price = last_quo and last_quo.price or self.goods_id.price
            self.tax_rate = self.goods_id.get_tax_rate(self.goods_id, self.partner_id, 'sell')            
            self.brand = self.goods_id.brand.name
            self.qty = self.goods_id.conversion    
            self.no_tax_price = self.price / (1 + self.tax_rate * 0.01)   
            self.delivery_days = self.goods_id.delivery_days 

    @api.onchange('price')
    def onchange_discount_rate(self):
        '''当数量、单价或优惠率发生变化时，优惠金额发生变化'''
        self.no_tax_price = self.price / (1 + self.tax_rate * 0.01)

    @api.onchange('no_tax_price', 'tax_rate')
    def onchange_price(self):
        '''当订单行的不含税单价改变时，改变含税单价。
        如果将含税价改为99,则self.price计算出来为84.62,price=99/1.17，
        跟84.62保留相同位数比较时是相等的，这种情况则保留含税价不变，
        这样处理是为了使得修改含税价时不再重新计算含税价。
        '''
        self.price = self.no_tax_price * (1 + self.tax_rate * 0.01)
